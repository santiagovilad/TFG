# How to run 

```
python train.py --name ttbar_particle_level_may  --data-path /lustrefs/hdd_pool_dir/eq_ntuples/minitrees/ttbar_training/     --analysis ttbar_pl  --data-format h5 --batch-size 1000 --lr 1e-4
```